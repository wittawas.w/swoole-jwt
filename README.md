# Swoole JWT

Example of JWT authentication with Swoole API

## Development
- Install swoole package
- Run `composer install`
- Run development server with `php server.php`

### Environment variables

Create `.env` file in your local directory and add these variables.
All values can be changed to match with your environment.

- PORT=9501
