<?php namespace ExampleWeb\API\V1;
  use \Firebase\JWT\JWT;

  class Token {
    function __construct($name) {
      $this->name = $name;
    }

    function name() {
      return $this -> name;
    }
  }
