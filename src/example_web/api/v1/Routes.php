<?php
  namespace ExampleWeb\API\V1;

  require(__DIR__.'/../../session/login.php');

  use ExampleWeb\Session\Login;
  use Siler\Http\Response;
  use Siler\Route;
  use Siler\Swoole;

  $handlerV1 = function () {
      Swoole\cors();
      Route\get('/', fn() => Response\json('Hello, World!'));
      Route\post('/login', new Login());
  };
