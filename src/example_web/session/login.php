<?php
  namespace ExampleWeb\Session;

  use Siler\Swoole;
  use Siler\Encoder;
  use Siler\Http\Request;
  use Firebase\JWT\JWT;

  class Login {
    public function __invoke() {
      $empty = [
        'error' => true,
        'message' => 'Empty',
      ];

      $error = [
        'error' => true,
        'message' => 'Unauthorized',
      ];

      $params = Request\params();

      if (empty($params)) {
        Swoole\json($empty);
        return;
      }

      if ($params['username'] !== 'admin' || $params['password'] !== 'admin') {
        Swoole\json($error);
        return;
       }

      $payload = ['username' => $params['username']];
      $token = JWT::encode($payload, $_ENV['SECRET_KEY']);

      $success = [
        'error' => false,
        'data' => $token,
      ];

      Swoole\json($success);
    }
  }
