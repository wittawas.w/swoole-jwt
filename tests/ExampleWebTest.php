<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

final class ExampleWebTest extends TestCase
{
    public function testCorrectVersion(): void
    {
        $this->assertEquals(
            '0.0.1',
            ExampleWeb\ExampleWeb::VERSION
        );
    }

}
