<?php
  require('src/example_web.php');

  use Siler\Swoole;
  use ExampleWeb\API\V1 as V1;

  use Swoole\Coroutine;
  use Swoole\Database\RedisConfig;
  use Swoole\Database\RedisPool;
  use Swoole\Runtime;

  require __DIR__ . '/vendor/autoload.php';

  # Load .env if existed
  $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
  $dotenv->safeLoad();

  Runtime::enableCoroutine();
  $redisPool = getRedisPool();

  Coroutine\run(function () use ($redisPool) {
    Coroutine::create(function () use ($redisPool) {
      $redis = $redisPool->get();
      $result = $redis->set('foo', 'bar');
      if (!$result) {
        throw new RuntimeException('Set failed');
      }
      $result = $redis->get('foo');
      if ($result !== 'bar') {
        throw new RuntimeException('Get failed');
      }
      $redisPool->put($redis);
    });
  });


  $port = $_ENV['PORT'];
  echo "Listening on port $port\n";
  Swoole\http($handlerV1, $port)->start();

  function getRedisPool() : RedisPool {
    $pool = new RedisPool((new RedisConfig)
      ->withHost('127.0.0.1')
      ->withPort(6379)
      ->withAuth('')
      ->withDbIndex(0)
      ->withTimeout(1)
    );

    return $pool;
  }


